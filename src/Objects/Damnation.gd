extends Position2D

export var player_path: NodePath
export var resident_path: NodePath
export var damnation_scene: PackedScene

onready var player: KinematicBody2D = get_node(player_path)
onready var resident: KinematicBody2D = get_node(resident_path)
onready var root = get_tree().get_root()
onready var default_time_wait = $StartTimer.wait_time
export var random_range = 20
	
func _on_StartTimer_timeout() -> void:
	$AttackTimer.start()
	print("start attack player. Player on the %s" % player.position)

func _get_configuration_warning() -> String:
	return "The damnation scene property can't be empty" if not damnation_scene else ""

func _on_AttackTimer_timeout() -> void:
	print("Attack init. Player on the %s" % player.position)
	var instance = damnation_scene.instance()
	
	instance.position = player.position
	root.add_child(instance)
	var new_position = get_atack_position(instance)
	
	if not $Audio.is_playing():
		$Audio.play()
	instance.position = new_position

func get_atack_position(instance: Node2D):
	var lighting_frames: SpriteFrames = instance.a_sprite.frames
	var lighting_frame: Texture = lighting_frames.get_frame("default", 0)
	var l_width = lighting_frame.get_width()
	randomize()
	var rand_slide = int(rand_range(1, l_width))
	var _direction = -1 if rand_slide % 2 == 0 else 1
	var position_slide_x = _direction * l_width + rand_slide
	var slice_x
	if global.game_finished:
		slice_x = player.position.x + position_slide_x + 50
	else:
		slice_x = (player.position.x - l_width / 2) + position_slide_x
	return Vector2(slice_x, -lighting_frame.get_height() / 2.0)
	
func _process(_delta: float) -> void:
	if (
		PlayerData.time_start != 0 
		and $StartTimer.is_stopped() 
		and $AttackTimer.is_stopped()
	):
		$StartTimer.start(default_time_wait + PlayerData.score)
	if PlayerData.time_start == 0 and not $AttackTimer.is_stopped():
		$AttackTimer.stop()


func _on_Resident_enter_target_point() -> void:
	var instance = damnation_scene.instance()
	instance.position.x = resident.position.x
	root.add_child(instance)
