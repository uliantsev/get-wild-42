extends Node2D

onready var a_sprite: AnimatedSprite = get_node("AnimatedSprite")
onready var splash_layer: ColorRect = get_node("CanvasLayer/ColorRect")

const FLASH_FRAME = 7
func _ready() -> void:
	start()

func start() -> void:
	print("start ligtin")
	a_sprite.frame = 0
	a_sprite.play()

func _on_frame_changed() -> void:
	if a_sprite.frame == FLASH_FRAME:
		splash_layer.visible = true
	else:
		splash_layer.visible = false


func _on_animation_finished() -> void:
	queue_free()
