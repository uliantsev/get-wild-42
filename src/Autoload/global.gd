extends Node

var started_player = false
var started_resident = false
var finish = false setget set_finish
var game_finished = false


func set_finish(value: bool) -> void:
	game_finished = value
	if value:
		emit_signal("finished_game")
