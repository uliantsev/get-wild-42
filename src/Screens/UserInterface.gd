extends Control

onready var scene_tree: = get_tree()
onready var timer_label: Label = get_node("Label")
const TIMER_TEXT = "Wild timer: %d.%1d"
export var altar_advice_path: NodePath
onready var altar_advice: Node = get_node(altar_advice_path)
export var  folk_palace_advice_path: NodePath
onready var folk_palace: Node = get_node(folk_palace_advice_path)

func _ready() -> void:
	pass

func _process(_delta: float) -> void:
	var time_outside: int
	if PlayerData.time_start == 0:
		time_outside = 0
		timer_label.text = ""
	else:
		if not global.game_finished:
			time_outside = OS.get_ticks_msec() - PlayerData.time_start
		var outside_sec = int(time_outside / 1000.0)
		var time_msc = int(time_outside / 100.0) % 10
		timer_label.text = TIMER_TEXT % [outside_sec, time_msc]


func _on_Player_area_entered(area) -> void:
	pass


func _on_Player_body_entered(body) -> void:
	match body:
		altar_advice:
			$TimerAdvice.start(3)
			$AviceTitle.text = "Altar of Ancient God"
		folk_palace:
			$TimerAdvice.start(3)
			$AviceTitle.text = "Wild folks"
	print(body)

func _on_Player_body_exit(_body) -> void:
	$TimerAdvice.start(0.5)

func _on_TimerAdvice_timeout() -> void:
	$AviceTitle.text = ""

func _on_Player_area_exit(_area) -> void:
	$TimerAdvice.start(0.5)


func _on_Resident_enter_target_point() -> void:
	pass # Replace with function body.


func _on_Welcome_dialogic_signal(value) -> void:
	match value:
		"started_player":
			global.started_player = true
		"started_resident":
			global.started_resident = true
