extends Actor

export var next_players_path: NodePath
onready var next_players: Node = get_node(next_players_path)
onready var a_sprite: AnimatedSprite = get_node("AnimatedSprite")

signal smoothing_finished
signal lose
signal area_entered(area)
signal area_exit(area)
signal body_entered(body)
signal body_exit(body)

func _get_configuration_warning() -> String:
	return "The next players property can't be empty" if not next_players else ""

func _ready() -> void:
	PlayerData.player = self

func _physics_process(_delta: float) -> void:
	
	if Input.get_action_strength("apply"):
		var areas = $ApplyArea.get_overlapping_areas()
		for area in areas:
			PlayerData.linked_npc = area.get_parent()
			print("non player applyed")
			break
		
	if is_live:
		move()
	check_smoothing_complete()
	return

func get_direction() -> Vector2:
	if not global.started_player or global.game_finished:
		$AnimatedSprite.play("idle")
		$Footstep.stop()
		return Vector2.ZERO
	if not $Footstep.is_playing():
		$Footstep.play()
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		-1.0 if Input.get_action_strength("jump") and is_on_floor() else 1.0
	)
	
func _on_AdviceDetector_area_entered(area: Area2D) -> void:
	emit_signal("area_entered", area)

func _on_AdviceDetector_area_exited(area: Area2D) -> void:
	emit_signal("area_exit", area)
	
func _on_AdviceDetector_body_entered(body: Node) -> void:
	emit_signal("body_entered", body)

func _on_AdviceDetector_body_exited(body: Node) -> void:
	emit_signal("body_exit", body)

func _on_OutsideDetector_body_entered(_body: Node) -> void:
	if PlayerData.time_start == 0:
		PlayerData.time_start = OS.get_ticks_msec()

func _on_InsideDetector_body_entered(_body: Node) -> void:
	PlayerData.time_start = 0

func _on_LigtingDetector_area_entered(_area: Area2D) -> void:
	print("Lighting detect")
	die()

func move() -> void:
	var is_jump_interrupted: = Input.is_action_just_released("jump") and _velocity.y < 0.0
	var direction: = get_direction()
	_velocity = calc_move_velocity(_velocity, direction, speed, is_jump_interrupted)
	var snap: = Vector2.DOWN * 50 if direction.y == 0.0 else Vector2.ZERO
	_velocity.y = move_and_slide_with_snap(
		_velocity, snap, FLOOR_NORMAL, true, 4, PI / 3.0
	).y
	if direction == Vector2.LEFT + Vector2.DOWN:
		a_sprite.play("run")
		a_sprite.flip_h = true
	elif direction == Vector2.RIGHT + Vector2.DOWN:
		a_sprite.play("run")
		a_sprite.flip_h = false
	elif is_live:
		$Footstep.stop()
		a_sprite.play("idle")
	return
	
func check_smoothing_complete():
	if (
		$Camera2D.smoothing_enabled
		and abs($Camera2D.get_camera_screen_center().x - position.x) < 5	
	):
		emit_signal("smoothing_finished")
		$Camera2D.smoothing_enabled = false

func calc_move_velocity(
		linear_velocity: Vector2, 
		direction: Vector2,
		speed: Vector2,
		is_jump_interrupted: bool
	) -> Vector2:
	var out: = linear_velocity
	out.x = speed.x * direction.x
	out.y += gravity * get_physics_process_delta_time()
	if direction.y == -1.0:
		out.y = speed.y * direction.y
	if is_jump_interrupted:
		out.y = 0.0
	return out
	
func die() -> void:
	if global.game_finished:
		return
	is_live = false
	a_sprite.play("death")
	$Footstep.stop()
	if PlayerData.linked_npc != null:
		PlayerData.linked_npc.die()
#	if not $DieSound.is_playing():
#		$DieSound.play()
	yield(a_sprite, "animation_finished")
	$Camera2D.smoothing_enabled = true
	if next_players.get_child_count():
		var _child: Actor = next_players.get_child(0)
		position.x = _child.position.x
		var a_sprite_child: AnimatedSprite = _child.get_node("AnimatedSprite")
		a_sprite.flip_h = a_sprite_child.flip_h
		yield(self, "smoothing_finished")
		is_live = true
		_child.queue_free()
	else:
		emit_signal("lose")
