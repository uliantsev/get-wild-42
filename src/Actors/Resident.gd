extends Actor

onready var started_position: Vector2 = position
export var target_position: Vector2 = position
var target_side
signal enter_target_point

func _ready() -> void:
	target_side = 1 if target_position.x > 0 else -1

func _physics_process(delta: float) -> void:
	if not global.started_resident:
		$AnimatedSprite.play("idle")
		return
	
	_velocity.y += gravity * delta
	
	if target_position.x >= position.x and is_live:
		_velocity.x = position.x
		speed.x = 0
		emit_signal("enter_target_point")
		die()
	elif is_live:
		_velocity.x += (speed.x * delta) * target_side
	
	if _velocity.x > 0.5 or _velocity.x < -0.5 and is_live:
		$AnimatedSprite.play("run")
	elif is_live:
		$AnimatedSprite.play("idle")
	if _velocity.x > 0:
		$AnimatedSprite.flip_h = false
	elif _velocity.x < 0:
		$AnimatedSprite.flip_h = true
		
	if is_live:
		var snap: = Vector2.DOWN * 50
		_velocity.y = move_and_slide_with_snap(
			_velocity, snap, FLOOR_NORMAL, true, 4, PI / 3.0
		).y

func die() -> void:
	is_live = false
	$AnimatedSprite.play("death")
	yield($AnimatedSprite, "animation_finished")
