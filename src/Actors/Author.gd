extends Actor

export var finish_dialog_scene: PackedScene
onready var root = get_tree().get_root()

func _on_FinishDetector_body_entered(body: Node) -> void:
	var instance = finish_dialog_scene.instance()
	root.add_child(instance)
	global.finish = true
