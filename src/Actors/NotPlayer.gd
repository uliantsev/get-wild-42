extends Actor

onready var started_position: Vector2 = position
export var size_walk: = 50


func _physics_process(delta: float) -> void:
	if not is_live:
		return
	_velocity.y += gravity * delta
		
	if self == PlayerData.linked_npc:
		_velocity.x = speed.x * (PlayerData.player.position.x - position.x)
	
	if _velocity.x > 0.5 or _velocity.x < -0.5 and is_live:
		$AnimatedSprite.play("walk")
	else:
		$AnimatedSprite.play("idle")
	if _velocity.x > 0:
		$AnimatedSprite.flip_h = false
	elif _velocity.x < 0:
		$AnimatedSprite.flip_h = true
		
		
	var snap: = Vector2.DOWN * 50
	_velocity.y = move_and_slide_with_snap(
		_velocity, snap, FLOOR_NORMAL, true, 4, PI / 3.0
	).y

func die() -> void:
	is_live = false
	print("die npc")
#	if not $DieSound.is_playing():
#		$DieSound.play()
	$AnimatedSprite.play("death")
	
	yield($AnimatedSprite, "animation_finished")
	if self == PlayerData.linked_npc:
		PlayerData.linked_npc = null
	queue_free()

func _on_AltarDetector_body_entered(_body: Node) -> void:
	die()
	PlayerData.score += 1


func _on_DieSound_finished() -> void:
	$DieSound.stop()
